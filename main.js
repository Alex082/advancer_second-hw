/*

1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

    Робота с: файлами (txt), 
              масивами или списками,
              сетевыми запросами.
*/

const books = [
    { 
      author: "Люсі Фолі ",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк ",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур ",
      name: "Неономікон ",
      price: 70
    }, 
    {
     author: "Террі Пратчетт ",
     name: "Рухомі картинки ",
     price: 40
    },
    {
     author: "Анґус Гайленд ",
     name: "Коти в мистецтві",
    }
  ];

  const rootElement = document.getElementById("root");
  const ulElement = document.createElement("ul");

  books.forEach(function(book) {
    try{
        if(!book.author) {
            throw new Error("Помилка: відсутний author");
        }

        if(!book.name) {
            throw new Error("Помилка: відсутне name");
        }

        if(!book.price) {
            throw new Error("Помилка: відсутний price");
        }

        const liElement = document.createElement("li");
        const bookText = document.createTextNode(
            `Автор: ${book.author}, Название: ${book.name}, Цена: ${book.price}`);

            liElement.appendChild(bookText);
            ulElement.appendChild(liElement);
    } catch(e) {
        console.log(e.message);
    }
  });

  rootElement.appendChild(ulElement);